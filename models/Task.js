const mongoose = require('mongoose');

// Schema- blueprint for our data/ document

const taskSchema = new mongoose.Schema({

	name: String,

	status: String
});

// Mongoose Model
/*
	mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)

*/

module.exports = mongoose.model("Task", taskSchema);

// module.exports allows us to export files/functions and be able to import/ require them in another files within our application

// Export model into other files thay maya require it.
