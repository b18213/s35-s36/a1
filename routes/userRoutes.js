const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userController');

router.post('/', userControllers.createUserControllers);

router.get('/', userControllers.getAllUsersControllers);

// Activity 2

router.put('/updateUsername/:id', userControllers.updateUsernameController)

router.get('/getSingleUser/:id', userControllers.getSingleUserController);

module.exports = router;