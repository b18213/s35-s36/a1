// import the Task model in our controllers. So that our controllers or controller functions may have acces to our Task model
const Task = require('../models/Task');

// Create task

module.exports.createTaskControllers = (req,res) =>{

	console.log(req.body);

	// mongo: db.tasks.findOne({name: "nameOfTask"})
	Task.findOne({name: req.body.name}).then(result => {

		if (result !== null && result.name ===req.body.name){
			return res.send('Duplicate task found')
		} else {

			let newTask = new Task({
				name: req.body.name,
				status: req.body.status
			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));

		}	
	})
	.catch(error => res.send(error));
};

// Retrieve ALL Tasks

module.exports.getAllTasksController = (req,res) => {

	Task.find({})
	.then(result => res.send(result))
	.catch(error =>res.send(error));
};



// retrieval single task

module.exports.getSingleTaskController = (req,res) => {

	console.log(req.params);

	// db.tasks.findOne({id: "id"})
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


module.exports.updateTaskStatusController = (req,res) =>{

	console.log(req.params.id);
	console.log(req.body);
	
	let updates = {
		status : req.body.status
	};

	// findByIdandUpdate
		// 3 arguments
			// a. where will you get the id?
			// b. what is the update? updates variable
			// c.optional: {new: true} -- return the updated version of the document we are updating
	Task.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err));
};

