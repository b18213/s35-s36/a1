// required modules
const express = require('express');

/*
	it allows to connect in our mongoDB
*/
const mongoose = require('mongoose');

// port
const port = 4000;

// server
const app = express();


// mongoose connection

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.wxavv.mongodb.net/task182?retryWrites=true&w=majority",
	{
		useNewUrlParser 	: true,
		useUnifiedTopology: true
	}
);
// this will create a notification if the db connection is successful or not
let db = mongoose.connection

db.on('error', console.error.bind(console, "DB Connection ERROR"));

db.once('open', () => console.log('Successfully connected to MongoDB'));
	

// middlewares
app.use(express.json());

// reading of data forms
// usually string or array are being accepted, with this middleware, this will enable us to accept other data types
app.use(express.urlencoded({extended: true}))

// Routes
const taskRoutes = require('./routes/taskRoutes');
app.use('/tasks', taskRoutes)

// Activity
const userRoutes = require('./routes/userRoutes');
app.use('/Users', userRoutes) 

// port listener
app.listen(port, () => console.log(`Server is running at ${port}`));